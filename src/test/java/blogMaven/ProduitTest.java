package blogMaven;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Tests de la classe Produit")
class ProduitTest {

	
	private Produit produit1;
	private Produit produit2;
	
	@BeforeEach
	void setUp() throws Exception {
		produit1 = new Produit(1, "café", "de l'eau chaud avec des grains de café", 100, 50, 8, true);
		produit2 = new Produit(2, "chocolat", "du lait et du chocolat", 200, 100, 25, true);
	}


	
	@Test
	void calculePoucentagePromoTest() {
		assertThat(50).isEqualTo(produit1.calculePourcentagePromo());
		assertThat(50).isEqualTo(produit2.calculePourcentagePromo());
	}
	
	@Test
	void isActiveOrInactiveTest() {
		assertThat(true).isEqualTo(produit1.activer());
		assertThat(false).isEqualTo(produit2.desactiver());
	}
	
	@Test
	void decommanderTest() {
		
	}
}
