package blogMaven;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

@DisplayName("Tests de la classe Person")
@Tag("PersonTest")
class PersonTest {

	private Person person1;
	private Person person2;

	@BeforeEach
	void initPersons() {

		person1 = new Person(1, "namePerson1", "firstnamePerson1", 23, "log1", "pwd1", false);
		person2 = new Person(2, "namePerson2", "firstnamePerson2", 2000, "log2", "pwd2", true);
	}

	@Test
	void getNomCompletTest() {
		String namep1 = "firstnamePerson1 namePerson1";

		assertThat(namep1).isEqualTo(person1.getNomComplet());
	}

	@Test
	void getDateNaisseTest() {

		assertThat(2000).isEqualTo(person1.getDateNaisse());
	}

	@Nested
	class Authentification {
		
		@Test
		void seConnecterTest() {
			String log1 = "log1";
			String pwd1 = "pwd1";

			assertTrue(person1.seConnecter(log1, pwd1));
		}

		@Test
		void seDeconnecterTest() {

			person2.seDeconnecter();
			assertFalse(person2.isOnline());

		}

	}

	@ParameterizedTest(name = "nom : {0} prenom : {1} nom complet : {2}")
	@CsvSource({ "namePerson1,firstnamePerson1,firstnamePerson1 namePerson1",
			"namePerson2,firstnamePerson2,firstnamePerson2 namePerson2",
			"namePerson3,firstnamePerson3,firstnamePerson3 namePerson3" })
	/*@ParameterizedTest
	@MethodSource("provideData")*/
	public void getNomCompletParam(String nom, String prenom, String nomComplet) {

		Person personToTest = new Person(0, nom, prenom, 0, null, null, false);
		assertThat(nomComplet).isEqualTo(personToTest.getNomComplet());

	}
	@ParameterizedTest
	@MethodSource("provideData")
	public void getNomCompletParamMethod(Person p, String nomComplet) {
		assertThat(nomComplet).isEqualTo(p.getNomComplet());
	}
	
	private static Stream<Arguments> provideData(){
		return Stream.of(
				Arguments.arguments(new Person(10), "firstname10 name10"),
				Arguments.arguments(new Person(12), "firstname12 name12"),
				Arguments.arguments(new Person(11), "firstname11 name11"),
				Arguments.arguments(new Person(13), "firstname13 name13")
				
				);
	}

}
