package blogMaven;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Tests de la classe Commande")
class OrderTest {
	
	private Order order1;
	private Order order2;
	private Produit produit1;
	private Produit produit2;

	@BeforeEach
	void initOrders() {
		produit1 = new Produit(1, "café", "de l'eau chaud avec des grains de café", 100, 50, 8, true);
		produit2 = new Produit(2, "chocolat", "du lait et du chocolat", 200, 100, 25, true);
		order1 = new Order(1, 35, false);
		order2 = new Order(2, 900, true);
	}

	@Test
	void ajoutProduitTest() {
		order1.ajoutProduit(produit1);
		assertThat(7).isEqualTo(produit1.getQuantite());
	}
	
	@Test
	void validationCommandeTest() {
		assertTrue(order2.validerCommande());
	}
	

}