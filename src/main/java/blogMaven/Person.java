package blogMaven;

public class Person {
	private int id;
	private String nom;
	private String prenom;
	private int age;
	private String login;
	private String motDePasse;
	private boolean isOnline;

	protected boolean isOnline() {
		return isOnline;
	}

	protected void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}
	

	public Person(int id) {
		super();
		this.id = id;
		this.nom = "name" + id;
		this.prenom = "firstname" + id;
	}

	public Person(int id, String nom, String prenom, int age, String login, String motDePasse, boolean isOnline) {
		super();

		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.login = login;
		this.motDePasse = motDePasse;
		this.isOnline = isOnline;
	}

	public String getNomComplet() {
		// TODO Auto-generated method stub
		return this.prenom + " " + this.nom;
	}

	public int getDateNaisse() {
		// TODO Auto-generated method stub
		return 2023 - this.age;
	}

	public boolean seConnecter(String log, String pwd) {
		// TODO Auto-generated method stub
		if(log.equals(login) && pwd.equals(motDePasse)) {
			isOnline = true;
		}
		return isOnline;
	}

	public void seDeconnecter() {
		// TODO Auto-generated method stub
		isOnline = false;

	}

}
