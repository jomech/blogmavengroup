package blogMaven;

public class Produit {
	
	private int id;
	private String libelle;
	private String description;
	private double prix_normal;
	private double prix_promo;
	private int quantite;
	private boolean isActive;



	public Produit(int id, String libelle, String description, double prix_normal, double prix_promo, int quantite,
			boolean isActive) {
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		this.prix_normal = prix_normal;
		this.prix_promo = prix_promo;
		this.quantite = quantite;
		this.isActive = isActive;
	}


	public double calculePourcentagePromo() {
		
		return ( prix_promo / prix_normal) * 100;
	}
	
	public boolean activer() {
		this.isActive = true;
		return isActive;
	}

	public boolean desactiver() {
		this.isActive = false;
		return isActive;
	}


	public int getQuantite() {
		return quantite;
	}
	
}
